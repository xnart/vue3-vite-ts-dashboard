import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import "bootstrap/dist/css/bootstrap.css";
import "./assets/scss/paper-dashboard.scss";

createApp(App).use(router).mount("#app");
