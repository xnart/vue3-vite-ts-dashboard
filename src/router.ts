import { createWebHistory, createRouter } from "vue-router";
import Dashboard from "./pages/Dashboard.vue";
import Other from "./pages/Other.vue";
const history = createWebHistory();
const routes = [
  { path: "/", component: Dashboard, meta: { pageTitle: "Paper Dashboard 2" } },
  { path: "/:pathMatch(.*)*", component: Other, meta: { pageTitle: "Title" } },
];
const router = createRouter({ history, routes });
export default router;
