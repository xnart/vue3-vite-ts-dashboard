import { reactive, toRefs } from "vue";

const state = reactive({
  showSidebar: false,
});

export default function useSidebar() {
  const toggleSidebar = () => {
    state.showSidebar = !state.showSidebar;
  };

  return {
    ...toRefs(state),
    toggleSidebar,
  };
}
