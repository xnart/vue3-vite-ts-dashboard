import { reactive, toRefs } from "vue";

const url = "http://localhost:3001/";

const state = reactive({
  data: { stats: {} },
  loading: true,
});

export default function useDashboard() {
  const fetchData = async () => {
    state.loading = true;
    state.data = await (await fetch(url)).json();
    state.loading = false;
  };

  return {
    ...toRefs(state),
    fetchData,
  };
}
